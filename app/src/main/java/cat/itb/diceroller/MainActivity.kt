package cat.itb.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.Toast.makeText
import com.google.android.material.snackbar.Snackbar
import android.os.Build
import android.view.View
import android.widget.*
import android.media.MediaPlayer





class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resetButton: Button
    lateinit var resDau1: ImageButton
    lateinit var resDau2: ImageButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rollButton = findViewById(R.id.roll_button)
        resetButton = findViewById(R.id.reset_button)

        resDau1 = findViewById(R.id.imageView2)
        resDau2 = findViewById(R.id.imageView3)

        val ring: MediaPlayer = MediaPlayer.create(this, R.raw.tirada)

        rollButton.setOnClickListener {
            ring.start()
            val rnds = (1..6).random()
            val rnds2 = (1..6).random()

            when (rnds){
                1 -> resDau1.setImageResource(R.drawable.dice_1)
                2 -> resDau1.setImageResource(R.drawable.dice_2)
                3 -> resDau1.setImageResource(R.drawable.dice_3)
                4 -> resDau1.setImageResource(R.drawable.dice_4)
                5 -> resDau1.setImageResource(R.drawable.dice_5)
                6 -> resDau1.setImageResource(R.drawable.dice_6)
            }
            when (rnds2){
                1 -> resDau2.setImageResource(R.drawable.dice_1)
                2 -> resDau2.setImageResource(R.drawable.dice_2)
                3 -> resDau2.setImageResource(R.drawable.dice_3)
                4 -> resDau2.setImageResource(R.drawable.dice_4)
                5 -> resDau2.setImageResource(R.drawable.dice_5)
                6 -> resDau2.setImageResource(R.drawable.dice_6)
            }

            if (rnds == 6 && rnds2 == 6) {
                val text = "JACKPOT!"
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(applicationContext, text, duration)
                toast.setGravity(Gravity.TOP, 0, 0)
                toast.show()

                /*

                val snackbar = Snackbar.make(
                    findViewById(R.id.principal_layout),
                    "JACKPOT!",
                    Snackbar.LENGTH_SHORT
                );
                val mView: View = snackbar.getView()
                val mTextView = mView.findViewById(R.id.snackbar_layout) as RelativeLayout
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) mTextView.textAlignment =
                    View.TEXT_ALIGNMENT_CENTER else mTextView.gravity =
                    Gravity.CENTER_HORIZONTAL

                snackbar.show()
                 */
            }
        }

        resetButton.setOnClickListener {
            resDau1.setImageResource(R.drawable.empty_dice)
            resDau2.setImageResource(R.drawable.empty_dice)
        }

        resDau1.setOnClickListener {
            val resultat = (1..6).random()
            when (resultat){
                1 -> resDau1.setImageResource(R.drawable.dice_1)
                2 -> resDau1.setImageResource(R.drawable.dice_2)
                3 -> resDau1.setImageResource(R.drawable.dice_3)
                4 -> resDau1.setImageResource(R.drawable.dice_4)
                5 -> resDau1.setImageResource(R.drawable.dice_5)
                6 -> resDau1.setImageResource(R.drawable.dice_6)
            }
        }

        resDau2.setOnClickListener {
            val resultat = (1..6).random()
            when (resultat){
                1 -> resDau2.setImageResource(R.drawable.dice_1)
                2 -> resDau2.setImageResource(R.drawable.dice_2)
                3 -> resDau2.setImageResource(R.drawable.dice_3)
                4 -> resDau2.setImageResource(R.drawable.dice_4)
                5 -> resDau2.setImageResource(R.drawable.dice_5)
                6 -> resDau2.setImageResource(R.drawable.dice_6)
            }
        }
    }
}
